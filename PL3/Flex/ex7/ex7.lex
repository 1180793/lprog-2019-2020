%{
   int numNum=0;
%}

%option noyywrap
%option nounput
%option noinput

DIGITO    [0-9]
NUMERO    [-+]?{DIGITO}*\.{DIGITO}+
/* alternativa para reconhecer . ou , como separador decimal
NUMERO    [-+]?{DIGITO}*[.,]{DIGITO}+
*/

%%

{NUMERO}  {  numNum++; printf("Numero: %s\n",yytext); }
.|\n       /* ignorado */

%%

int main()
{
  yylex();
  printf("Num. de Números: %d\n",numNum);

  return 0;
}
