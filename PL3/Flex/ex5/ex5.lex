%{
   int numNum=0;
%}

%option noyywrap
%option nounput
%option noinput

DIGITO    [0-9]
NUMERONAT {DIGITO}+

%%

{NUMERONAT}  {  numNum++; printf("%s ",yytext); }
.            /* ignorado */

%%

int main()
{
  yylex() ;
  printf("Num. de Numeros Naturais: %d\n",numNum);

  return 0;
}
