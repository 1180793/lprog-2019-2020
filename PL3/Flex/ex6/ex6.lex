%{
   int numNum=0;
%}

%option noyywrap
%option nounput
%option noinput

DIGITO    [0-9]
INTEIRO   [-+]?{DIGITO}+

%%

{INTEIRO}  {  numNum++; printf("Numero: %s\n",yytext); }
.|\n       /* ignorado */

%%

int main()
{
  yylex();
  printf("Num. de Números: %d\n",numNum);

  return 0;
}
