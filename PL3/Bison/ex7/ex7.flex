%{
  #include "ex7.tab.h"   /* header gerado pelo bison */
%}

%option noyywrap
%option nounput
%option noinput

%%

[0-9]{7}           yylval.id=strdup(yytext); return NUMALUNO;
20|1[0-9]|[0-9]    yylval.inteiro=atol(yytext); return NOTA;
[1-9][A-Z][A-Z]    yylval.id=strdup(yytext); return TURMA;
[ND\n]             return yytext[0];
[ \t]              /*ignorado*/
.               {
                 printf("Erro lexico: simbolo desconhecido %s\n",yytext);
                 numErros++;
                }
<<EOF>>            return 0;

%%
