%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  /* header file gerado pelo flex */
  #include "ex7.h"

  int numErros=0;

  char *nomeTmp=NULL;

  void yyerror(const char *s){
      numErros++;
      printf("\nErro sintactico/semantico: %s\n",s);
  }

  char *procura(long numero) {
      const char *nomes[]={"João ", "Maria ", "Manuel ",
            "Carla ", "Joana ", "António ", "Pedro ",
            "Carlos ", "Simão ", "Clara ", "Luísa "};
      const char *apelidos[]={"Santos", "Silva", "Batista",
            "Monteiro", "Figueiredo", "Sá", "Marinho",
            "Madureira", "Tavares", "Martins" };
      char* nome;
      int n=rand()%(sizeof(nomes)/sizeof(char*));
      int a=rand()%(sizeof(apelidos)/sizeof(char*));
      nome=(char*) malloc(n+a+1);
      strcpy(nome,nomes[n]);
      strcat(nome,apelidos[a]);
      return(nome);
  }
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  extern int numErros;
  void yyerror(const char *s);
}

%union {
    float real;
    long  inteiro;
    char *id;
}

%token <id> TURMA
%token <inteiro> NOTA NUMALUNO

%type <id> tipo aluno turma

%destructor {free($$);} TURMA tipo aluno turma

%start inicio

%debug
 /* em versões do bison menores que 3.0.x usar
    %error-verbose
 */
%define parse.error verbose

%%
inicio: /* vazio */
      | inicio linha '\n'
      | inicio error '\n'
;

linha: tipo NOTA NOTA aluno turma {
              printf("O(A) aluno(a) %s da turma %s com frequencia em regime %s:\n  Teve notas de Frequência: %ld, Exame:%ld e Final:",$4, $5, $1, $2, $3);

              if ($2<8 || $3<8)
                  printf("SM\n  Resultado final: reprovado\n\n");
              else
                  printf("%.0lf\n  Resultado final: %s\n\n", \
                      round($2*0.45+$3*0.55), \
                      (round($2*0.45+$3*0.55)<10)?"reprovado":"aprovado");


          }
;
tipo: 'D' {$$=strdup("Diurno");}
    | 'N' {$$=strdup("Noturno");}
;

aluno: NUMALUNO {$$=procura($1);}
;
turma: TURMA {$$=strdup($1);}
;

%%

int main(){
  yyparse();

  return 0;
}
