%{
  #include "ex5b.tab.h"   /* header gerado pelo bison */
%}

%option noyywrap
%option nounput
%option noinput

DIGITO   [0-9]
LETRA    [A-Za-z]
INTEIRO  {DIGITO}+
REAL     ({DIGITO}*".")?{DIGITO}+([eE][-+]?{DIGITO}+)?

%%

exit                return EXIT;
{INTEIRO}           yylval.inteiro=atoi(yytext);return INTEIRO;
{REAL}              yylval.real=atof(yytext);return REAL;
[a-z]               yylval.inteiro= yytext[0] - 'a'; return ID;
[-+()*/=\n]         return yytext[0];
[ \t\r]+            /* ignorado */
.                   printf("Erro lexico: %s\n",yytext);numErros++;
<<EOF>>             return 0;

%%
