%{
  /* header file gerado pelo flex */
  #include "ex5a.h"

  double tabelasimb[26];  /* tabela de simbolos para guardar variaveis */
  int numErros=0;

  void yyerror(const char *s){
      numErros++;
      printf("\nErro sintactico/semantico: %s\n",s);
  }
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  extern int numErros;
  void yyerror(const char *s);
}

%union {
  double   real;
  int      inteiro;
}

%token  EXIT
%token  <inteiro> INTEIRO ID
%token  <real> REAL

%type  <real> termo factor final

%debug
 /* em versões do bison menores que 3.0.x usar
    %error-verbose
 */
%define parse.error verbose

%%

calculadora: /* vazio */
           | calculadora expressao '\n'
           | calculadora EXIT '\n'      {return 0;}
           | calculadora error '\n'
;

expressao: termo         {printf(">>resultado %f\n", $1);}
         | ID '=' termo  {printf(">>guardado %f\n", $3);
                          tabelasimb[$1]=$3;
                         }
;

termo:  termo '+' factor  {$$=$1+$3;}
     | termo '-' factor  {$$=$1-$3;}
     | factor    {$$=$1;} /* pode-se omitir, acção por omissão */
;

factor: factor '*' final {$$=$1*$3;}
      | factor '/' final {if ($3==0)
                            yyerror("divisao por zero");
                          else
                            $$=$1/$3;
                         }
      | final            {$$=$1;} /* pode-se omitir, acção por omissão */
;

final: '-' final     {$$=-$2;}
     | '(' termo ')' {$$=$2;}
     | REAL          {$$=$1;}
     | INTEIRO       {$$=$1;}
     | ID            {$$=tabelasimb[$1];}
;

%%

int main(){
  printf("Calculadora com 26 memórias (a-z)\n");
  printf("Exemplo de utilização (exit para sair):\n");
  printf("a=10+20*3-1\n");
  printf("10*a\n");
  yyparse();

  return 0;
}
