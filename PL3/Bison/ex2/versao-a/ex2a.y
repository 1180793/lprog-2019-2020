%{
  #include <stdio.h>
  #include "ex2a.h"   /* header gerado pelo flex */

  int numErros=0;

  void yyerror(const char *s){
      numErros++;
      printf("erro sintactico/semantico: %s\n",s);
  }

  void compara(int condicao){
        if (condicao)
           printf("verdadeiro\n");
        else
           printf("falso\n");
  }
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  void yyerror(const char *s);
}

%token INT DIF MEN_IG MAI_IG

%start inicio

%debug
  /* em versões do bison menores que 3.0.x usar
     %error-verbose
  */
%define parse.error verbose

%%

inicio:   /* vazio */
        | inicio comparacao '\n'
        | inicio error '\n'
    ;

comparacao:
          INT '<' INT     {printf("%d<%d: ",$1,$3);compara($1<$3);}
        | INT '>' INT     {printf("%d>%d: ",$1,$3);compara($1>$3);}
        | INT '=' INT     {printf("%d=%d: ",$1,$3);compara($1==$3);}
        | INT DIF INT     {printf("%d<>%d: ",$1,$3);compara($1!=$3);}
        | INT MAI_IG INT  {printf("%d>=%d: ",$1,$3);compara($1>=$3);}
        | INT MEN_IG INT  {printf("%d<=%d: ",$1,$3);compara($1<=$3);}
   ;

%%

int main(){
  yyparse();
  printf("Número de erros: %d\n",numErros);
  return 0;
}
