%{
  #include <stdio.h>
  #include "ex2b.h"      /* header flex */
  #include "ex2b.tab.h"  /* header bison para usar tokens na função compara */

  int numErros=0;

  void yyerror(const char *s){
      numErros++;
      printf("erro sintactico/semantico: %s\n",s);
  }

  void compara(int n1, int op, int n2){
    int condicao;
    switch (op) {
       case '<' : printf("%d<%d: ",n1,n2);condicao=(n1<n2); break;
       case '>' : printf("%d>%d: ",n1,n2);condicao=(n1>n2); break;
       case '=' : printf("%d=%d: ",n1,n2);condicao=(n1==n2); break;
       case DIF : printf("%d<>%d: ",n1,n2);condicao=(n1!=n2); break;
       case MEN_IG : printf("%d<=%d: ",n1,n2);condicao=(n1<=n2); break;
       case MAI_IG : printf("%d>=%d: ",n1,n2);condicao=(n1>=n2); break;
    }

    if (condicao)
       printf("verdadeiro\n");
    else
       printf("falso\n");
  }
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  void yyerror(const char *s);
}

%token INT DIF MEN_IG MAI_IG

%start inicio

%debug
  /* em versões do bison menores que 3.0.x usar
     %error-verbose
  */
%define parse.error verbose

%%
inicio:   /* vazio */
        | inicio comparacao '\n'
        | inicio error '\n'
    ;

comparacao: INT operador INT      {compara($1, $2, $3);}
   ;

operador:   /* podemos atribuir os tokens ao $$ pois são inteiros */
        '<'       {$$='<';}
      | '>'       {$$='>';}
      | '='       {$$='=';}
      | DIF       {$$=DIF;}
      | MAI_IG    {$$=MAI_IG;}
      | MEN_IG    {$$=MEN_IG;}
   ;

%%

int main(){
  yyparse();
  printf("Número de erros: %d\n",numErros);
  return 0;
}
