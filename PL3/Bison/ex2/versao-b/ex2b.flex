%{
  #include "ex2b.tab.h" /* header gerado pelo bison */
%}

%option noyywrap
%option noinput
%option nounput

%%

[-+]?[0-9]+      yylval=atoi(yytext); return INT;
[<>=\n]          return yytext[0];
"<>"             return DIF;
"<="             return MEN_IG;
">="             return MAI_IG;
[ \t]            /* ignorado */
.                yyerror("Erro Lexico");
<<EOF>>          return 0;

%%
