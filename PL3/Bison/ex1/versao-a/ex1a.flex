%{
  #include "ex1a.tab.h"   /* header gerado pelo bison */
  extern int numErros;    /* variável criada no bison */
%}

%option noyywrap
%option nounput
%option noinput
%option case-insensitive

%%

hello     return HELLO;
world     return WORLD;
[ \t]     /* ignorado */
.        {printf("Erro Lexico: simbolo desconhecido %s\n",yytext);
          numErros++;
         }
\n       return 0;  /* Como só queremos hello world \n termina yylex */
<<EOF>>  return 0;

%%
