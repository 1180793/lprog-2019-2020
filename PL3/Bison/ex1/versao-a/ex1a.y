%{
  #include <stdio.h>
  #include "ex1a.h"
  int numErros=0;

  void yyerror(const char *s){
    numErros++;
    printf("Erro sintatico/semantico: %s\n",s);
  }
%}

%token HELLO WORLD
%start inicio

%debug
  /* em versões do bison menores que 3.0.x usar
     %error-verbose
  */
%define parse.error verbose

%%

inicio: HELLO WORLD {printf("Hello World!!!\n");}
    ;

%%

int main(){
  yyparse();
  if(numErros==0)
    printf("Frase válida\n");
  else
    printf("Frase inválida\nNúmero de erros: %d\n",numErros);
  return 0;
}
