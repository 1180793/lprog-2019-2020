%{
  #include <stdio.h>
  #include "ex1b.h"

  int numErros=0;

  void yyerror(const char *s){
    numErros++;
    printf("erro sintactico/semantico: %s\n",s);
  }
%}

%token HELLO WORLD PALAVRA
%start inicio

%debug
  /* em versões do bison menores que 3.0.x usar
     %error-verbose
  */
%define parse.error verbose

%%

inicio:   /* vazio */
        | inicio HELLO WORLD {printf("Hello World!!!\n");}
        | inicio PALAVRA
        | inicio HELLO       {printf("Hello sem World\n");}
        | inicio WORLD       {printf("World sem Hello\n");}
    ;


%%

int main(){
  yyparse();

  return 0;
}
