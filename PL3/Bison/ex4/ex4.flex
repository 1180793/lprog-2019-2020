%{
  #include <stdlib.h>
  #include "ex4.tab.h" // header greado pelo bison
%}

%option noyywrap
%option nounput
%option noinput

ID      [A-Za-z_][A-Za-z0-9_]*
INTEIRO [-+]?[0-9]+
REAL    [-+]?[0-9]"."[0-9]+([eE][+-]?[0-9]+)?

%%

int               return INT;
char              return CHAR;
float             return FLOAT;
double            return DOUBLE;
short             return SHORT;
long              return LONG;
unsigned          return UNSIGNED;
{ID}              yylval.id=strdup(yytext); return ID;
{INTEIRO}         yylval.inteiro=atoi(yytext); return INTEIRO;
{REAL}            yylval.real=atof(yytext); return REAL;
'.'               yylval.inteiro=yytext[1]; return CARACTER;
[;=,]             return yytext[0];
[ \t\n]           /* ignorado */
<<EOF>>           return 0;

%%
