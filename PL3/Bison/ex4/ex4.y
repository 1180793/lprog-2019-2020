%{
  #include <stdio.h>
  /* header file gerado pelo flex */
  #include "ex4.h"

  int numErros=0;

  void yyerror(const char *s){
      numErros++;
      printf("\nErro sintactico/semantico: %s\n",s);
  }

%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  extern int numErros;
  void yyerror(const char *s);
}

/* Neste programa não necessitamos dos lexemas, mas estão a seguir
   passados numa union para compreenderem melhor o mecanismo
   essencialmente para lexemas char*    */
%union {
  char *id;
  int   inteiro;
  float real;
}

%token INT CHAR FLOAT DOUBLE SHORT LONG UNSIGNED
%token <inteiro> INTEIRO CARACTER
%token <real>    REAL
%token <id>      ID

/* para libertar memória reservada com o strdup devemos fazer
   free da memória ocupada pelo lexema, quando já não é usado.
   Podemos libertar a memória de uma das seguintes formas:
      - nas acções semanticas das regras que usam esses tokens,
        com {free($x);} em que x é numero de ordem do token;
      - usando um %destructor e colocando todos os tokens e regras
        que precisam deste destructor (neste caso é só o ID)
*/

%destructor {free($$);} ID

%start declaracoes

%%
declaracoes: /* vazio */
      | declaracao declaracoes
;

declaracao: tipo lista_variaveis ';' {printf("Declaracao valida!\n");}
          | error ';'
;

tipo: modifier tipo_base
    ;

tipo_base: INT
         | CHAR
         | FLOAT
         | DOUBLE
;

modifier: /* vazio */
        | SHORT
        | LONG
        | UNSIGNED
;

lista_variaveis: variavel
               | lista_variaveis  ',' variavel
;

variavel: ID atribuicao
;

atribuicao: /* vazio */
          | '=' valor
;

valor: ID
     | INTEIRO
     | REAL
     | CARACTER
;

%%

int main(){
  yyparse();
  printf("Número de erros: %d\n",numErros);
  return 0;
}
