%{
  #include <stdio.h>
  /* header file gerado pelo flex */
  #include "ex3a.h"
  /* header file gerado pelo bison para usar tokens na funcao compara */
  #include "ex3a.tab.h"

  int numErros=0;
  int tipo, tipoAnt;

  void yyerror(const char *s){
      numErros++;
      printf("\nErro sintactico/semantico: %s\n",s);
  }

void compara(int n1, int op, int n2){
  int condicao;
  if(tipo!=tipoAnt)
    printf("incompativeis ");
  else {
    switch (op) {
      case '<' : condicao=(n1<n2); break;
      case '>' : condicao=(n1>n2); break;
      case '=' : condicao=(n1==n2); break;
      case DIF : condicao=(n1!=n2); break;
      case MEN_IG : condicao=(n1<=n2); break;
      case MAI_IG : condicao=(n1>=n2); break;
    }
    if (condicao)
      printf("verdadeiro ");
    else
      printf("falso ");
  }
}
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  void yyerror(const char *s);
}

%token INT CHAR DIF MEN_IG MAI_IG
%start inicio
%debug
 /* em versões do bison menores que 3.0.x usar
    %error-verbose
 */
%define parse.error verbose

%%
inicio:   /* vazio */
        | inicio comparacao '\n'  {printf("\n");}
        | inicio error '\n'      {printf("\n");}
    ;

comparacao: valor            {$$=$1;tipoAnt=tipo;}
      | comparacao operador valor
                             {compara($1,$2,$3);
                              $$=$3;tipoAnt=tipo;
                             }
      | comparacao valor {yyerror("Falta operador de comparação"); $$=$2;}
      | error            {$$=-1;tipoAnt=-1;}
  ;

operador: '<'       {$$='<';}
        | '>'       {$$='>';}
        | '='       {$$='=';}
        | DIF       {$$=DIF;}
        | MAI_IG    {$$=MAI_IG;}
        | MEN_IG    {$$=MEN_IG;}
  ;

valor: CHAR     {$$=$1; tipo=CHAR;}
     | INT      {$$=$1; tipo=INT;}
  ;

%%

int main(){
  yyparse();

  printf("Número de erros: %d\n",numErros);
  return 0;
}
