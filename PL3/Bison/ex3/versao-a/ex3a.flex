%{
  #include "ex3a.tab.h" /* header gerado pelo bison */
%}

%option noyywrap
%option noinput
%option nounput

%%


[-+]?[0-9]+   yylval=atoi(yytext); return INT;
[a-zA-Z]      yylval=yytext[0]; return CHAR;
[<>=\n]       return yytext[0];
"<>"          return DIF;
"<="          return MEN_IG;
">="          return MAI_IG;
[ \t]         /* ignorado */
.             yyerror("Erro Lexico");
<<EOF>>       return 0;

%%
