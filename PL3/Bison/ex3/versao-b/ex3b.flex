%{
  #include "ex3b.tab.h" /* header gerado pelo bison */
%}

%option noyywrap
%option noinput
%option nounput

%%

[-+]?[0-9]+     {yylval.tipo.valor=atoi(yytext);
                 yylval.tipo.tipo=INT;
                 return INT;
                }
[a-zA-Z]        {yylval.tipo.valor=yytext[0];
                 yylval.tipo.tipo=CHAR;
                 return CHAR;
                }
[<>=\n]         return yytext[0];
"<>"            return DIF;
"<="            return MEN_IG;
">="            return MAI_IG;
[ \t]           /* ignorado */
.               yyerror("Erro Lexico");
<<EOF>>         return 0;

%%
