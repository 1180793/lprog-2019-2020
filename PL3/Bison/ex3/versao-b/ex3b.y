%{
  #include <stdio.h>
  /* header file gerado pelo flex */
  #include "ex3b.h"
  /* header file gerado pelo bison para usar tokens na funcao compara */
  #include "ex3b.tab.h"

  int numErros=0;

  void yyerror(const char *s){
      numErros++;
      printf("\nErro sintactico/semantico: %s\n",s);
  }

  void compara(int n1, int op, int n2){
      int condicao;
    switch (op) {
      case '<' : condicao=(n1<n2); break;
      case '>' : condicao=(n1>n2); break;
      case '=' : condicao=(n1==n2); break;
      case DIF : condicao=(n1!=n2); break;
      case MEN_IG : condicao=(n1<=n2); break;
      case MAI_IG : condicao=(n1>=n2); break;
    }

      if (condicao)
        printf("verdadeiro ");
      else
        printf("falso ");
  }
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  void yyerror(const char *s);
}

/* campo tipo da union vai ser uma struct com 2 campos (tipo e valor) */
%union {
  struct {
    int tipo;
    int valor;
  } tipo;
  int inteiro;
}

%token <tipo> INT CHAR
%token DIF MEN_IG MAI_IG

%type <tipo> comparacao valor
%type <inteiro> operador

%start inicio

%debug
 /* em versões do bison menores que 3.0.x usar
    %error-verbose
 */
%define parse.error verbose

%%
inicio:   /* vazio */
        | inicio comparacao '\n'  {printf("\n");}
        | inicio error '\n'       {printf("\n");}
    ;

comparacao: valor            {$$=$1;}
      | comparacao operador valor
                    {if($1.tipo!=$3.tipo)
                        printf("incompativeis ");
                     else
                       compara($1.valor,$2,$3.valor);
                     $$=$3;
                    }
      | comparacao valor {yyerror("Falta operador de comparação"); $$=$2;}
      | error            {$$.valor=0;$$.tipo=-1;}
  ;

operador: '<'       {$$='<';}
        | '>'       {$$='>';}
        | '='       {$$='=';}
        | DIF       {$$=DIF;}
        | MAI_IG    {$$=MAI_IG;}
        | MEN_IG    {$$=MEN_IG;}
  ;

valor: CHAR     {$$=$1;}
     | INT      {$$=$1;}
  ;

%%

int main(){
  yyparse();
  printf("Número de erros: %d\n",numErros);
  return 0;
}
