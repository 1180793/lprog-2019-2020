%{
  #include <stdio.h>
  #include <math.h>   /* round */
  #include <strings.h>
  #include"ex6.h"

  int numErros=0, QtdMoedas=0;

  #define YYSTYPE double

  void yyerror(const char *s){
      numErros++;
      printf("erro sintactico/semantico: %s\n",s);
  }


  void troco(double preco, double dinheiro)
  {
    int moedas[]={200,100,50,20,10,5,2,1};
    int i,troco,num_moedas=sizeof(moedas)/sizeof(int);

    troco=round((dinheiro-preco)*100);   /* int por causa do erro de arredondamento */
    printf("(troco: €%.2f)",troco/100.0);
    if (troco<0)
      printf(", Dinheiro insuficiente");
    else
      if (troco==0)
        printf(", Não há troco");
      else
        for(i=0;i<num_moedas;i++){
          while(troco>=moedas[i])
          {
            printf (", €%.2f",moedas[i]/100.0);
            troco-=moedas[i];
          }
        }
    printf("\n");
  }
%}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex
   no bloco inicial de código %{   %}
*/

%code provides {
  extern int numErros;
  void yyerror(const char *s);
}

/* Se usarem uma versão do bison menor que 3.0.x,
   colocar o código a seguir no ficheiro flex e bison
   no bloco inicial de código %{   %}
*/
%code requires {
  #define YYSTYPE double
}


%token CAFE PINGO CHA CHOCOLATE LEITE COPO BYE
%token MOEDA

%debug
 /* em versões do bison menores que 3.0.x usar
    %error-verbose
 */
%define parse.error verbose

%start maquina

%%
maquina: /* vazio */
     | maquina pedido
  ;
pedido:  bebida ',' lista_moedas '\n' {troco($1,$3);
                     printf("Foram introduzidas %d moedas \n",QtdMoedas);
                    }
    | BYE   '\n'    {printf("obrigado pelas compras....\n");return 0;}
    | error '\n'
  ;
lista_moedas:  MOEDA          {$$=$1;QtdMoedas=1;}
     | lista_moedas ',' MOEDA  {$$=$1+$3;QtdMoedas++;}
;

bebida: CAFE        {printf("cafe €0.35 ");$$=.35;}
  | PINGO           {printf("pingo €0.35 ");$$=.35;}
  | CHA             {printf("cha €0.35 ");$$=.35;}
  | CHOCOLATE       {printf("chocolate  €0.40 ");$$=.40;}
  | LEITE           {printf("leite €0.30 ");$$=.30;}
  | COPO            {printf("copo €0.05 ");$$=.05;}
;

%%

int main() {
  printf("máquina de venda de produtos (bye para sair)\n");
  printf("Produtos disponiveis: cafe, pingo, cha, chocolate, leite, copo\n");
  printf("Moedas aceites: €2.00, €1.00, €050, €0.20, €0.10, €0.05, €0.02, €0.01\n");
  printf("insira o produto seguido da lista de moedas\n");
  printf("ex: cafe, €2.00, €0.01, €0.05, €0.02\n");

  yyparse();
  return 0;
}
