%{
  #include <stdio.h>
  #include "ex6.tab.h" /* header gerado pelo bison */
%}

%option noyywrap
%option noinput
%option nounput
%option case-insensitive

%%
              /* em UTF-8 o simbolo do euro ocupa 3 chars */
€(0\.(0[125]|[125]0)|[12]\.00)  {yylval=atof(yytext+3);return MOEDA;}
€([0-9]*\.[0-9]*)        {printf("Moeda Inválida '%s'\n",yytext);numErros++;}
cafe                  {return CAFE;}
pingo                 {return PINGO;}
cha                   {return CHA;}
chocolate             {return CHOCOLATE;}
leite                 {return LEITE;}
copo                  {return COPO;}
BYE                   {return BYE;}
\n|,                  {return(yytext[0]);}
[ \t\r]               /* ignorado */
<<EOF>>               {return 0;}
.                     {printf("Erro lexico '%s'\n",yytext);numErros++;}
%%
