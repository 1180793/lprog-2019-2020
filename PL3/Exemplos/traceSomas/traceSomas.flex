%{
  #include"traceSomas.tab.h" /* header gerado pelo bison */
  extern int numErros;
%}

%option noyywrap
%option nounput
%option noinput

%%

[0-9]+           yylval=atoi(yytext);return INT;
[+]              return yytext[0];
[ \t]            /* ignorado */
.               {
                 printf("Erro lexico: simbolo desconhecido %s\n",yytext);
                 numErros++;
                }
\n               return 0; /* tratar \n como final de input */
<<EOF>>          return 0;

%%
