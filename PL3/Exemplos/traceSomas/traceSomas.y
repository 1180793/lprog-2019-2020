%{
  #include <stdio.h>
  #include "traceSomas.h"
  int numErros=0;

  void yyerror(const char *s){
    numErros++;
    printf("erro sintactico/semantico: %s\n",s);
  }

%}

%token INT

%printer {fprintf (yyo, "%d",$$);} INT soma

%start inicio

%debug
  /* em versões do bison menores que 3.0.x usar
     %error-verbose
  */
%define parse.error verbose

%%

inicio: soma           {printf("Total: %d",$1);}
;
soma: soma '+' INT     {$$=$1+$3;}
    | INT              {$$=$1;}
;

%%

int main(){
  yydebug=1;
  yyparse();
  if(numErros==0)
    printf("Frase válida\n");
  else
    printf("Frase inválida\nNúmero de erros: %d\n",numErros);
  return 0;
}
