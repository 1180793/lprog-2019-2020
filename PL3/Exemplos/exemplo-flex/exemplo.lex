%{
/*	#include<time.h>*/
	 int numChars=0,numAlgarismos=0,numLinhas=0;
%}

%option nounput
%option noinput
%option noyywrap

ALGARISMO		[0-9]

%%

{ALGARISMO}		numAlgarismos++;

.	{
       numChars++;
      /* printf("%s",yytext);*/
	}

\n {
	numLinhas++;
       /*printf("\n");*/
   }

%%

int main() {
	yylex( ) ;
    printf("Número de caracteres %d\n" , numChars);
    printf("Número de algarismos %d\n" , numAlgarismos);
    printf("Número de linhas %d\n" , numLinhas);
	return 0;
}
