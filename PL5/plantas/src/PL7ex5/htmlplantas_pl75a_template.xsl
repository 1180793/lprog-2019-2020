<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns="http://www.isep.ipp.pt/lprog"
                version="1.0">
    
    <xsl:output method="html"/>

    <xsl:template match="/">

        <html>
            <head>
                <title>Plantas</title>
            </head>
            <body>
                <!-- 5a1 -->
                Nome comum da segunda planta = 
                <xsl:value-of select="ns:Catalogo/ns:Planta[2]/ns:NomeComum"/>
                 
                <!-- Versão completa do filtro para a segunda planta -->  
                <xsl:value-of select="ns:Catalogo/ns:Planta[position()=2]/ns:NomeComum"/>
                <br/> 
                
                <!-- 5a2 -->
                Quantidade de plantas = 

                <br/> 
                
                <!-- 5a3 -->
                Preço da última planta = 

                <br/> 
                
                <!-- 5a4 -->
                Preço Médio = 

                <br/> 

                <br/> 
                                
                <!-- 5a5 -->
                Preço Médio (arredondado) = 

                <br/> 

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
