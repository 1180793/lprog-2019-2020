{
        
        "Catalogo" : { "Planta" :[{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Bloodroot",
        "NomeBotanico" : "Sanguinaria canadensis",
        "Zona" : "4",
        "Preco" : "2.44",
        "Disponibilidade" : "031599"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Columbine",
        "NomeBotanico" : "Aquilegia canadensis",
        "Zona" : "3",
        "Preco" : "9.37",
        "Disponibilidade" : "030699"
                },{
                "NecessidadeLuz" : "Preferencialmente Sol",
    
        "NomeComum" : "Marsh Marigold",
        "NomeBotanico" : "Caltha palustris",
        "Zona" : "4",
        "Preco" : "6.81",
        "Disponibilidade" : "051799"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Cowslip",
        "NomeBotanico" : "Caltha palustris",
        "Zona" : "4",
        "Preco" : "9.90",
        "Disponibilidade" : "030699"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Dutchman's-Breeches",
        "NomeBotanico" : "Diecentra cucullaria",
        "Zona" : "3",
        "Preco" : "6.44",
        "Disponibilidade" : "012099"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Ginger, Wild",
        "NomeBotanico" : "Asarum canadense",
        "Zona" : "3",
        "Preco" : "9.03",
        "Disponibilidade" : "041899"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Hepatica",
        "NomeBotanico" : "Hepatica americana",
        "Zona" : "4",
        "Preco" : "4.45",
        "Disponibilidade" : "012699"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Liverleaf",
        "NomeBotanico" : "Hepatica americana",
        "Zona" : "4",
        "Preco" : "3.99",
        "Disponibilidade" : "010299"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Jack-In-The-Pulpit",
        "NomeBotanico" : "Arisaema triphyllum",
        "Zona" : "4",
        "Preco" : "3.23",
        "Disponibilidade" : "020199"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Mayapple",
        "NomeBotanico" : "Podophyllum peltatum",
        "Zona" : "3",
        "Preco" : "2.98",
        "Disponibilidade" : "060599"
                },{
                "NecessidadeLuz" : "Sol ou Sombra",
    
        "NomeComum" : "Phlox, Woodland",
        "NomeBotanico" : "Phlox divaricata",
        "Zona" : "3",
        "Preco" : "2.80",
        "Disponibilidade" : "012299"
                },{
                "NecessidadeLuz" : "Sol ou Sombra",
    
        "NomeComum" : "Phlox, Blue",
        "NomeBotanico" : "Phlox divaricata",
        "Zona" : "3",
        "Preco" : "5.59",
        "Disponibilidade" : "021699"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Spring-Beauty",
        "NomeBotanico" : "Claytonia Virginica",
        "Zona" : "7",
        "Preco" : "6.59",
        "Disponibilidade" : "020199"
                },{
                "NecessidadeLuz" : "Sol ou Sombra",
    
        "NomeComum" : "Trillium",
        "NomeBotanico" : "Trillium grandiflorum",
        "Zona" : "5",
        "Preco" : "3.90",
        "Disponibilidade" : "042999"
                },{
                "NecessidadeLuz" : "Sol ou Sombra",
    
        "NomeComum" : "Wake Robin",
        "NomeBotanico" : "Trillium grandiflorum",
        "Zona" : "5",
        "Preco" : "3.20",
        "Disponibilidade" : "022199"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Violet, Dog-Tooth",
        "NomeBotanico" : "Erythronium americanum",
        "Zona" : "4",
        "Preco" : "9.04",
        "Disponibilidade" : "020199"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Trout Lily",
        "NomeBotanico" : "Erythronium americanum",
        "Zona" : "4",
        "Preco" : "6.94",
        "Disponibilidade" : "032499"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Adder's-Tongue",
        "NomeBotanico" : "Erythronium americanum",
        "Zona" : "4",
        "Preco" : "9.58",
        "Disponibilidade" : "041399"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Anemone",
        "NomeBotanico" : "Anemone blanda",
        "Zona" : "6",
        "Preco" : "8.86",
        "Disponibilidade" : "122698"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Grecian Windflower",
        "NomeBotanico" : "Anemone blanda",
        "Zona" : "6",
        "Preco" : "9.16",
        "Disponibilidade" : "071099"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Bee Balm",
        "NomeBotanico" : "Monarda didyma",
        "Zona" : "4",
        "Preco" : "4.59",
        "Disponibilidade" : "050399"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Bergamont",
        "NomeBotanico" : "Monarda didyma",
        "Zona" : "4",
        "Preco" : "7.16",
        "Disponibilidade" : "042799"
                },{
                "NecessidadeLuz" : "Sol",
    
        "NomeComum" : "Black-Eyed Susan",
        "NomeBotanico" : "Rudbeckia hirta",
        "Zona" : "Annual",
        "Preco" : "9.80",
        "Disponibilidade" : "061899"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Buttercup",
        "NomeBotanico" : "Ranunculus",
        "Zona" : "4",
        "Preco" : "2.57",
        "Disponibilidade" : "061099"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Crowfoot",
        "NomeBotanico" : "Ranunculus",
        "Zona" : "4",
        "Preco" : "9.34",
        "Disponibilidade" : "040399"
                },{
                "NecessidadeLuz" : "Sol",
    
        "NomeComum" : "Butterfly Weed",
        "NomeBotanico" : "Asclepias tuberosa",
        "Zona" : "Annual",
        "Preco" : "2.78",
        "Disponibilidade" : "063099"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Cinquefoil",
        "NomeBotanico" : "Potentilla",
        "Zona" : "Annual",
        "Preco" : "7.06",
        "Disponibilidade" : "052599"
                },{
                "NecessidadeLuz" : "Sol",
    
        "NomeComum" : "Primrose",
        "NomeBotanico" : "Oenothera",
        "Zona" : "3 - 5",
        "Preco" : "6.56",
        "Disponibilidade" : "013099"
                },{
                "NecessidadeLuz" : "Sol ou Sombra",
    
        "NomeComum" : "Gentian",
        "NomeBotanico" : "Gentiana",
        "Zona" : "4",
        "Preco" : "7.81",
        "Disponibilidade" : "051899"
                },{
                "NecessidadeLuz" : "Sol ou Sombra",
    
        "NomeComum" : "Blue Gentian",
        "NomeBotanico" : "Gentiana",
        "Zona" : "4",
        "Preco" : "8.56",
        "Disponibilidade" : "050299"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Jacob's Ladder",
        "NomeBotanico" : "Polemonium caeruleum",
        "Zona" : "Annual",
        "Preco" : "9.26",
        "Disponibilidade" : "022199"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Greek Valerian",
        "NomeBotanico" : "Polemonium caeruleum",
        "Zona" : "Annual",
        "Preco" : "4.36",
        "Disponibilidade" : "071499"
                },{
                "NecessidadeLuz" : "Sol",
    
        "NomeComum" : "California Poppy",
        "NomeBotanico" : "Eschscholzia californica",
        "Zona" : "Annual",
        "Preco" : "7.89",
        "Disponibilidade" : "032799"
                },{
                "NecessidadeLuz" : "Preferencialmente Sombra",
    
        "NomeComum" : "Shooting Star",
        "NomeBotanico" : "Dodecatheon",
        "Zona" : "Annual",
        "Preco" : "8.60",
        "Disponibilidade" : "051399"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Snakeroot",
        "NomeBotanico" : "Cimicifuga",
        "Zona" : "Annual",
        "Preco" : "5.63",
        "Disponibilidade" : "071199"
                },{
                "NecessidadeLuz" : "Sombra",
    
        "NomeComum" : "Cardinal Flower",
        "NomeBotanico" : "Lobelia cardinalis",
        "Zona" : "2",
        "Preco" : "3.02",
        "Disponibilidade" : "022299"
                }] }}
    