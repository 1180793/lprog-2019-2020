<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : xmlplantas.xsl
    Created on : 16 de Maio de 2011, 14:57
    Author     : jtavares
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>  <title>LPROG xslt - Exercicio 5b</title>      </head>
            <body>
                <textarea rows="100" cols="100">
                    <xsl:apply-templates select="Catalogo"/>
                </textarea>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Catalogo">
        <Plantas>
            <xsl:attribute name="Quantidade">
                <xsl:value-of select = "count(/Catalogo/Planta)"/>
            </xsl:attribute>
            
            <xsl:for-each select="Planta">
                <xsl:element name="Planta">
                    <xsl:attribute name="NomeCientifico">
                        <xsl:value-of select = "NomeBotanico"/>
                    </xsl:attribute>
                    <xsl:attribute name="Preco">
                        <xsl:value-of select = "Preco"/>
                    </xsl:attribute>
                    <xsl:element name="Necessidade">
                        <xsl:value-of select = "@NecessidadeLuz"/>
                    </xsl:element>
                </xsl:element>

            </xsl:for-each>
            
            
        </Plantas>
    </xsl:template>

</xsl:stylesheet>
