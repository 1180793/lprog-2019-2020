<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : htmlplantas.xsl
    Created on : 16 de Maio de 2011, 15:50
    Author     : jtavares
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    <xsl:template match="/">
        <html>
            <head>
                <title>LPROG xslt-Exercicio 5f</title>
            </head>
            <body>
                <table border="1" align="center">
                    <tr bgcolor="#9acd32">
                        <th>Zona</th>
                        <th># Plantas</th>
                    </tr>
                    <xsl:apply-templates select="Catalogo">
                        <xsl:sort select="Planta/Zona"/>
                    </xsl:apply-templates>
                </table>
            </body>
        </html>
    </xsl:template>
    
    <!-- 5f -->
    <xsl:template match="Catalogo">
        # Zonas =  
        <xsl:value-of select="count(Planta[not(Zona = preceding-sibling::Planta/Zona)])"/>
        <xsl:for-each select="Planta[not(Zona = preceding-sibling::Planta/Zona)]">
            <!-- <xsl:sort select="Zona"/> -->
            <tr>
                <td align="center">
                    <xsl:value-of select="Zona"/>
                </td>
                <td align="center">
                    <xsl:value-of select="count(/Catalogo/Planta[Zona=current()/Zona])"/>
                </td>
            </tr>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>
