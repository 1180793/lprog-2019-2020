<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : htmlplantas.xsl
    Created on : 16 de Maio de 2011, 15:50
    Author     : jtavares
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    <xsl:template match="/">
        <html>
            <head>
                <title>LPROG xslt-Exercicio 1</title>
            </head>
            <body>
                <table border="1" align="center">
                    <tr bgcolor="#9acd32">
                        <th></th>
                        <th>Nome</th>
                        <th>Preço</th>
                        <th>Disponibilidade</th>
                        <th>Necessidade Luz</th>
                    </tr>
                    <xsl:apply-templates select="Catalogo/Planta">
                        <xsl:sort select="NomeComum"/>
                    </xsl:apply-templates>
                </table>
            </body>
        </html>
    </xsl:template>
    
    <!-- 5d -->
    <!--xsl:template match="Catalogo">
        <xsl:apply-templates select="Planta">
            <xsl:sort select="NomeComum"/>
        </xsl:apply-templates>
        <xsl:for-each select="Planta">
            <xsl:sort select="NomeComum"/>
            <tr>
                <td align="center">
                    <xsl:value-of select="position()"/>
                </td>
                <td>
                    <xsl:value-of select="NomeComum"/>
                </td>
                <td align="center">
                    <xsl:value-of select="Preco"/>
                </td>
                <td align="center">
                    <xsl:value-of select="Disponibilidade"/>
                </td> 
                <td align="center">
                    <xsl:choose>
                        <xsl:when test="@NecessidadeLuz='Sol'">
                            <img src="sunny.png" alt="sunny" width="50" height="50"/>
                        </xsl:when>
                        <xsl:when test="@NecessidadeLuz='Sombra'">
                            <img src="cloudy.png" alt="cloudy" width="50" height="50"/>
                        </xsl:when>
                        <xsl:when test="@NecessidadeLuz='Preferencialmente Sombra'">
                            <img src="mostly_cloudy.png" alt="mostly cloudy" width="50" height="50"/>
                        </xsl:when>
                        <xsl:when test="@NecessidadeLuz='Sol ou Sombra'">
                            <img src="partly_sunny.png" alt="partly sunny" width="50" height="50"/>
                        </xsl:when>
                    </xsl:choose>
                    <br/>
                    <small> <xsl:value-of select="@NecessidadeLuz"/> </small>
                </td> 
            </tr>
        </xsl:for-each>
    </xsl:template-->

    
    <xsl:template match="Planta">
        <tr>
            <td align="center">
                <xsl:value-of select="position()"/>
            </td>
            <td>
                <xsl:value-of select="NomeComum"/>
            </td>
            <td align="center">
                <xsl:value-of select="Preco"/>
            </td>
            <td align="center">
                <xsl:value-of select="Disponibilidade"/>
            </td> 
            <td align="center">
                <xsl:choose>
                    <xsl:when test="@NecessidadeLuz='Sol'">
                        <img src="sunny.png" alt="sunny" width="50" height="50"/>
                    </xsl:when>
                    <xsl:when test="@NecessidadeLuz='Sombra'">
                        <img src="cloudy.png" alt="cloudy" width="50" height="50"/>
                    </xsl:when>
                    <xsl:when test="@NecessidadeLuz='Preferencialmente Sombra'">
                        <img src="mostly_cloudy.png" alt="mostly cloudy" width="50" height="50"/>
                    </xsl:when>
                    <xsl:when test="@NecessidadeLuz='Sol ou Sombra'">
                        <img src="partly_sunny.png" alt="partly sunny" width="50" height="50"/>
                    </xsl:when>
                </xsl:choose>
                <br/>
                <small> 
                    <xsl:value-of select="@NecessidadeLuz"/> 
                </small>
            </td> 
        </tr>
    </xsl:template>
</xsl:stylesheet>
