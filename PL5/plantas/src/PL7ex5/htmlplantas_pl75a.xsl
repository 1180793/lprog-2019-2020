<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : plantas.xsl
    Created on : 16 de Maio de 2011, 14:07
    Author     : jtavares
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns="http://www.isep.ipp.pt/lprog"
                version="3.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">

        <html>
            <head>
                <title>Plantas</title>
            </head>
            <body>
                <!-- 5a1 -->
                Nome comum da segunda planta = 
                <xsl:value-of select="ns:Catalogo/ns:Planta[2]/ns:NomeComum"/>
                
                <br/> 
                
                <!-- 5a2 -->
                Quantidade de plantas = 
                <xsl:value-of select="count(ns:Catalogo/ns:Planta)"/>

                <br/> 
                
                <!-- 5a3 -->
                Preço da última planta = 
                <xsl:value-of select="ns:Catalogo/ns:Planta[last()]/ns:Preco"/>
                
                <br/> 
                
                <!-- 5a4 -->
                Preço Médio = 
                <xsl:value-of select="sum(ns:Catalogo/ns:Planta/ns:Preco) div 
                    count(ns:Catalogo/ns:Planta)"/> 
                <br/> 
                
                <br/> 
                                
                <!-- 5a5 -->
                Preço Médio (arredondado) = 
                <xsl:value-of select='format-number((sum(ns:Catalogo/ns:Planta/ns:Preco) 
                    div count(ns:Catalogo/ns:Planta)), "#.00")'/> 

                <br/> 
                <br/> Exemplo de uso de 'format-number'
                <br/> 
                <xsl:value-of select='format-number(050.0100, "#")' />
                <br/>
                <xsl:value-of select='format-number(05.00100, "0")' />
                <br/>
                <xsl:value-of select='format-number(050010.0, "#.00")' />
                <br/>
                <xsl:value-of select='format-number(0500100, "#.0")' />
                <br/>
                <xsl:value-of select='format-number(0500100, "###,###.00")' />
                <br/>
                <xsl:value-of select='format-number(0.23456, "#%")' />
                
                <br/>
                <br/>
                <br/>  Primeira versão XPath 1.0 e XSLT 1.0 :  
                
                <xsl:value-of select="ns:Catalogo/ns:Planta[not (ns:Preco &lt; preceding-sibling::ns:Planta/ns:Preco or ns:Preco &lt; following-sibling::ns:Planta/ns:Preco)]/ns:NomeComum"/>

                <br/>  Segunda versão XPath 1.0 e XSLT 1.0 :  
                <xsl:value-of select="/ns:Catalogo/ns:Planta[not(ns:Preco &lt; /ns:Catalogo/ns:Planta/ns:Preco)]/ns:NomeComum"/>

                <br/> Versão usando XPath 2.0 e XSLT 2.0 : 
                <xsl:value-of select="/ns:Catalogo/ns:Planta[ns:Preco=max(/ns:Catalogo/ns:Planta/ns:Preco)]/ns:NomeComum"/>

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
