<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
    version="3.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:ns="http://www.isep.ipp.pt/lprog">

    <xsl:template match="/ns:Catalogo">
        <html>
            <head> 
                <title/>
            </head>
            <body>
                <h1 align="center">Listagem e contagem de plantas</h1>

                <!--Contar as zonas distintas:-->
                <h3 align="center">N� de zonas distintas: 
                    <xsl:value-of select="fn:count(fn:distinct-values(/ns:Catalogo/ns:Planta/ns:Zona))"/>
                    <br/>
                    <xsl:value-of select="fn:distinct-values(/ns:Catalogo/ns:Planta/ns:Zona)"/>
                    <!-- XSLT2.0 -->
                </h3>

                <table align="center" border="1">
                    <tr>
                        <th>ZONA</th>
                        <th>PLANTAS</th>
                        <th>Contagem</th>
                    </tr>
					
                    <!--Listar as zonas distintas v.XSLT2.0-->
                    <xsl:for-each-group select="ns:Planta" group-by="ns:Zona">
                        <tr>
                            <td>
                                <xsl:value-of select="ns:Zona"/>
                            </td>
                            <td>
                                <xsl:for-each select="current-group()" >
                                    <blockquote>
                                        <xsl:value-of select="ns:NomeComum" />
                                    </blockquote>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:value-of select="count(current-group())"/>
                                <br/>
                                + barata = <xsl:value-of select='current-group()[ns:Preco=min(current-group()/ns:Preco)]/ns:NomeComum'/>
                                � <xsl:value-of select='current-group()[ns:Preco=min(current-group()/ns:Preco)]/ns:Preco'/>
                            </td>
                        </tr>
                    </xsl:for-each-group>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
